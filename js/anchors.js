window.addEventListener("load", function (event) {
    const locationHash = window.location.hash.substr(1);

    if (locationHash) {
        scrollTo(locationHash);
    }

    initUpButton();
    initHashLinks();

    function initHashLinks() {
        const links = document.querySelectorAll('a[href^="#"]');

        for (let link of links) {
            link.addEventListener('click', function (e) {
                let blockID = link.getAttribute('href').substr(1);

                e.preventDefault();

                scrollTo(blockID);
            })
        }
    }

    function initUpButton() {
        const button = document.querySelector('#up_button');

        window.addEventListener('scroll', function () {
            if (window.scrollY >= 500) {
                button.style.display = 'inline-block'
            } else {
                button.style.display = 'none'
            }
        });
    }

    function scrollTo(blockId) {
        document.getElementById(blockId).scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        })
    }
}, false);