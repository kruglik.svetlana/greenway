window.addEventListener("load", function (event) {

    // Home Page Logic

    if (window.matchMedia("(min-width: 768px)").matches) {
        let video = document.querySelector('#video');

        video.setAttribute('src', video.getAttribute('data-src'));
    }

}, false);