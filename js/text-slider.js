window.addEventListener("load", function () {
    let sliderContainer = document.querySelector('#text_slider'),
        index = 0,
        timeOut = 5000,
        slidesList = [
            'Работа онлайн',
            'Экотовары для дома и семьи',
            'Бизнес без вложений'
        ];


    // Init new slider

    function InitSlider() {
        setInterval(function () {
            sliderContainer.innerHTML = '';

            setSlide(index);

            if (++index >= slidesList.length) {
                index = 0;
            }

        }, timeOut);
    }


    // Setting new slide

    function setSlide(index) {
        let slide = slidesList[index],
            i = 0,
            timer = setInterval(function () {
                appendCharacter(slide[i]);

                if (++i >= slide.length) {
                    clearInterval(timer);
                }
            }, 20);
    }


    // Appending new character into container

    function appendCharacter(char) {
        let child = document.createElement('span');

        child.innerHTML = char;

        sliderContainer.appendChild(child);
    }

    InitSlider();

}, false);