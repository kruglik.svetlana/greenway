window.addEventListener("load", function (event) {
    let xhttp = new XMLHttpRequest(),
        popUp = document.querySelector('#popup_container'),
        form = document.querySelector('#subscribe_form'),
        subscribeButton = document.querySelector('#subscribe_button');

    xhttp.onreadystatechange = function () {
        if (xhttp.readyState === XMLHttpRequest.DONE && xhttp.status === 200) {
            popUp.style.display = 'flex';
        }
    };

    // Subscribing form listener logic
    subscribeButton.addEventListener('click', function () {
        let formData;

        if (formValidate(form)) {
            formData = new FormData(form);

            xhttp.open("POST", "Controllers/telegram.php", true);
            xhttp.send(formData);
        }
    });

    function formValidate(form) {
        return form.reportValidity();
    }
}, false);