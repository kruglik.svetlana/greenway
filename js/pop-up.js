window.addEventListener("load", function (event) {
    let closeButton = document.querySelector('#close'),
        container = document.querySelector('#popup_container');

    closeButton.addEventListener('click', function () {
        container.style.display = 'none';
    })

    container.addEventListener('click', function (event) {
        if (event.target === container) {
            container.style.display = 'none';
        }
    })

}, false);